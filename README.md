# TASK MANAGER

## DEVELOPER INFO

* NAME: Yana Fuksina

* E-MAIL: ya.fuksi.na@gmail.com

## SOFTWARE

* OPENJDK 8

* Intellij Idea

* MS Windows 10

## HARDWARE

* RAM: 16Gb

* CPU: i5

* HDD: 512Gb

## RUN PROGRAM

```
java -jar task-manager.jar
```
