package ru.tsc.fuksina.tm;

import ru.tsc.fuksina.tm.constant.TerminalConst;

public class Application {

    public static void main(String[] args) {
        process(args);
    }

    public static void process(final String[] args) {
        if (args == null || args.length < 1) return;
        final String arg = args[0];
        switch (arg) {
            case TerminalConst.VERSION:
                showVersion();
                break;
            case TerminalConst.ABOUT:
                showAbout();
                break;
            case TerminalConst.HELP:
                showHelp();
                break;
            default:
                showError(arg);
                break;
        }
    }

    public static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.2.1");
    }

    public static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Author: Yana Fuksina");
        System.out.println("Email: ya.fuksi.na@gmail.com");
    }

    public static void showHelp() {
        System.out.println("[HELP]");
        System.out.printf("%s - Display program version.\n", TerminalConst.VERSION);
        System.out.printf("%s - Display developer info.\n", TerminalConst.ABOUT);
        System.out.printf("%s - Display list of terminal commands.\n", TerminalConst.HELP);
    }

    public static void showError(String arg) {
        System.err.printf("Error! Unknown command `%s`...", arg);
    }
}
